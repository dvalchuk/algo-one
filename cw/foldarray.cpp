#include <vector>
#include <algorithm>
#include <cassert>

#include <ostream>
#include <iostream>
#include <boost/detail/lightweight_test.hpp>

typedef std::vector<int> Data;
std::ostream& operator << (std::ostream& os, const Data& d);

class Kata
{
public:
    Data foldArray(Data array, int runs)
    {
        assert(runs > 0);
        assert(!array.empty());
        if (array.size() == 1)
        {
            return array;
        }
        if (runs == 1)
        {
            return fold(array);
        }
        else
        {
            return foldArray(fold(array), runs - 1);
        }
    }
private:
    Data fold(const Data& array)
    {
        const auto newSize = (array.size() + 1) / 2;
        Data result(newSize, 0);
        for(Data::size_type i = 0, sz = array.size() / 2; i < sz; ++i)
        {
            result[i] = array[i] + array[array.size() - 1 - i];
        }
        if (newSize > array.size() / 2)
        {
            result[newSize - 1] = array[newSize - 1];
        }
        return result;
    }
};


std::ostream& operator << (std::ostream& os, const Data& d)
{
    os << "[";
    for(int i: d)
    {
        os << i << ", ";
    }
    os << "]";
    return os;
}

int main(int, const char**)
{
    Kata kata;
    {
        const Data expected = { 6, 6, 3 };
        const Data actual = kata.foldArray({ 1, 2, 3, 4, 5 }, 1);
        BOOST_TEST_EQ(actual, expected);
    }
    {
        const Data expected = { 9, 6 };
        const Data actual = kata.foldArray({ 1, 2, 3, 4, 5 }, 2);
        BOOST_TEST_EQ(actual, expected);
    }
    {
        const Data expected = { 15 };
        const Data actual = kata.foldArray({ 1, 2, 3, 4, 5 }, 3);
        BOOST_TEST_EQ(actual, expected);
    }
    {
        const Data expected = { 14, 75, 0 };
        const Data actual = kata.foldArray({ -9, 9, -8, 8, 66, 23 }, 1);
        BOOST_TEST_EQ(actual, expected);
    }
    {
        const Data expected = { 15 };
        const Data actual = kata.foldArray({ 15 }, 3);
        BOOST_TEST_EQ(actual, expected);
    }
    return boost::report_errors();
}
