const assert = require('assert');

try
{
    function foldArray(array, runs)
    {
        var fold = function(array)
        {
            var newSize = Math.trunc((array.length + 1) / 2)
            var result = new Array(newSize).fill(0)
            for (var i = 0; i < (array.length / 2); i++)
            {
                result[i] = array[i] + array[array.length - 1 - i]
            }
            if (newSize > (array.length / 2))
            {
                result[newSize - 1] = array[newSize - 1]
            }
            return result
        }
        
        if (array.length == 1)
        {
            return array
        }
        if (runs == 1)
        {
            return fold(array)
        }
        else
        {
            return foldArray(fold(array), runs - 1)
        }

    }

    var expected = [ 6, 6, 3 ]
    var actual = foldArray([ 1, 2, 3, 4, 5 ], 1)
    assert.deepEqual(actual, expected)

    var expected = [ 9, 6 ]
    var actual = foldArray([ 1, 2, 3, 4, 5 ], 2)
    assert.deepEqual(actual, expected)

    var expected = [ 15 ]
    var actual = foldArray([ 1, 2, 3, 4, 5 ], 3)
    assert.deepEqual(actual, expected)

    var expected = [ 14, 75, 0 ]
    var actual = foldArray([ -9, 9, -8, 8, 66, 23 ], 1)
    assert.deepEqual(actual, expected)
        
    var expected = [ 15 ]
    var actual = foldArray([ 15 ], 3)
    assert.deepEqual(actual, expected)

    console.log('Ok')
}
catch (e)
{
    console.log(e)
}
