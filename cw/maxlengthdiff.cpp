#include <vector>
#include <string>
#include <algorithm>

#include <boost/detail/lightweight_test.hpp> 

class MaxDiffLength
{
public:
    static int mxdiflg(std::vector<std::string> &a1, std::vector<std::string> &a2);
};

int MaxDiffLength::mxdiflg(std::vector<std::string> &a1, std::vector<std::string> &a2)
{
    if (a1.empty() || a2.empty())
    {
        return -1;
    }
    auto cmp = [](const std::string& lhv, const std::string& rhv) { return lhv.size() < rhv.size(); };
    std::sort(begin(a1), end(a1), cmp);
    std::sort(begin(a2), end(a2), cmp);
    return std::max(a1.back().size() - a2.front().size(), a2.back().size() - a1.front().size());
}

int main(int, const char**)
{
    std::vector<std::string> a1 = {"hoqq", "bbllkw", "oox", "ejjuyyy", "plmiis", "xxxzgpsssa", "xxwwkktt", "znnnnfqknaz", "qqquuhii", "dvvvwz"};
    std::vector<std::string> a2 = {"cccooommaaqqoxii", "gggqaffhhh", "tttoowwwmmww"};
    BOOST_TEST_EQ(MaxDiffLength::mxdiflg(a1, a2), 13);
    a1 = {"ejjjjmmtthh", "zxxuueeg", "aanlljrrrxx", "dqqqaaabbb", "oocccffuucccjjjkkkjyyyeehh"};
    a2 = {"bbbaaayddqbbrrrv"};
    BOOST_TEST_EQ(MaxDiffLength::mxdiflg(a1, a2), 10);
    return boost::report_errors();
}
