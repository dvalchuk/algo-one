#include <limits>
#include <vector>
#include <cctype>

#include <string>
#include <algorithm>

#include <boost/detail/lightweight_test.hpp> 

namespace {

size_t duplicateCount(const char* in)
{
    size_t result = 0;
    std::vector<int> all(std::numeric_limits<unsigned char>::max()/* + 1*/, 0); // one char for '\0' is not used
    for (const char* c = in; *c != '\0'; ++c)
    {
        int lc = std::tolower(*c);
        if (all[lc] == 1)
        {
            result++;
        }
        all[lc]++;
    }
    return result;
}

size_t duplicateCount(const std::string& in)
{
    return duplicateCount(in.c_str());
}

}

int main()
{
    BOOST_TEST_EQ(duplicateCount(" "), 0);
    BOOST_TEST_EQ(duplicateCount(""), 0);
    BOOST_TEST_EQ(duplicateCount("asdfghjkl;'\\"), 0);
    BOOST_TEST_EQ(duplicateCount("asdfghjkl;'\\'"), 1);
    BOOST_TEST_EQ(duplicateCount("asdfghjkl;'\\qwertyuiop[]{}:\"|zxcvbnm,./<>?1234567890-=`~!@#$%^&*()_+ "), 0);
    BOOST_TEST_EQ(duplicateCount("abcde"), 0);
    BOOST_TEST_EQ(duplicateCount("aabbcde"), 2);
    BOOST_TEST_EQ(duplicateCount("aabBcde"), 2);
    BOOST_TEST_EQ(duplicateCount("Indivisibility"), 1);
    BOOST_TEST_EQ(duplicateCount("Indivisibilities"), 2);

    const std::string lowers("abcdefghijklmnopqrstuvwxyz");
    std::string uppers = lowers;
    std::transform(std::begin(uppers), std::end(uppers), uppers.begin(), toupper);
    BOOST_TEST_EQ(duplicateCount(lowers), 0);
    BOOST_TEST_EQ(duplicateCount(lowers + "baaAAB"), 2);
    return boost::report_errors();
}
