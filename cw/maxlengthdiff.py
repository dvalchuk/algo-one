import unittest

def mxdiflg(a1, a2):
    if not a1 or not a2:
        return -1
    cmplen = lambda lhv, rhv: cmp(len(lhv), len(rhv))
    a1s = sorted(a1, cmplen)
    a2s = sorted(a2, cmplen)
    return max(len(a1s[-1]) - len(a2s[0]), len(a2s[-1]) - len(a1s[0]))
    
class SolutionTester(unittest.TestCase):

    def test_mxdiflg(self):
        a1 = ["hoqq", "bbllkw", "oox", "ejjuyyy", "plmiis", "xxxzgpsssa", "xxwwkktt", "znnnnfqknaz", "qqquuhii", "dvvvwz"]
        a2 = ["cccooommaaqqoxii", "gggqaffhhh", "tttoowwwmmww"]
        self.assertEqual(mxdiflg(a1, a2), 13)
        a1 = ["ejjjjmmtthh", "zxxuueeg", "aanlljrrrxx", "dqqqaaabbb", "oocccffuucccjjjkkkjyyyeehh"]
        a2 = ["bbbaaayddqbbrrrv"]
        self.assertEqual(mxdiflg(a1, a2), 10)

if __name__ == "__main__":
    unittest.main()
