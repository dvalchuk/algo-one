const assert = require('assert')

class Collector
{
    constructor()
    {
        this.all = []
        this.min = Number.MAX_SAFE_INTEGER
        this.max = 0
        this.acc = 0
    }

    add(t)
    {
        if (t < this.min)
            this.min = t
        if (t > this.max)
            this.max = t
        this.acc += t
        this.all.push(t)
    }

    range()
    {
        return this.max - this.min
    }

    mean()
    {
        return Math.trunc(this.acc / this.all.length)
    }

    median()
    {
        let center = Math.trunc(this.all.length / 2)
        this.all.sort(function(a, b) { return a - b })
        return (this.all.length % 2) ? this.all[center] : Math.trunc((this.all[center - 1] + this.all[center]) / 2)
    }
}

function fixedNumLength(num, length) {
    let r = "" + num;
    while (r.length < length) {
        r = "0" + r;
    }
    return r;
}

function fromSeconds(ss)
{
    let h = Math.trunc(ss / (60 * 60))
    let mp = ss - h * 60 * 60
    let m = Math.trunc(mp / 60)
    let s = mp - m * 60
    return fixedNumLength(h, 2) + "|" + fixedNumLength(m, 2) + "|" + fixedNumLength(s, 2)
}

function stat(sin)
{
    if (!sin)
        return ""
    let cl = new Collector()
    let rx = /([0-9]{1,2})\|([0-9]{1,2})\|([0-9]{1,2})[,\s]{0,1}/ig
    let match
    while (match = rx.exec(sin))
    {
        let s = match[1] * 60 * 60
        s += match[2] * 60
        s += Number(match[3])
        cl.add(s)
    }
    let range = fromSeconds(cl.range())
    let mean = fromSeconds(cl.mean())
    let median = fromSeconds(cl.median())
    return "Range: " + range + " Average: " + mean + " Median: " + median
}

try
{
    assert.deepEqual(stat("01|15|59, 1|47|16, 01|17|20, 1|32|34, 2|17|17"),
                     "Range: 01|01|18 Average: 01|38|05 Median: 01|32|34")
    assert.deepEqual(stat("02|15|59, 2|47|16, 02|17|20, 2|32|34, 2|17|17, 2|22|00, 2|31|41"),
                               "Range: 00|31|17 Average: 02|26|18 Median: 02|22|00")
    assert.deepEqual(stat(""), "")

    console.log('Ok')
}
catch (e)
{
    console.log(e)
}
