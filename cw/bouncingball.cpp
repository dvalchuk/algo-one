#include <boost/detail/lightweight_test.hpp>

using namespace std;

class Bouncingball
{
public:
    static int bouncingBall(double h, double bounce, double window);
};

int Bouncingball::bouncingBall(double h, double bounce, double window)
{
    if (bounce <= 0 || bounce >= 1.0 || h <= 0 || window >= h)
    {
        return -1;
    }
    int result = 1; // down
    const double remaining = h * bounce;
    if (remaining > window)
    {
        ++result; // up
        return result + Bouncingball::bouncingBall(remaining, bounce, window);
    }
    return result;
}

int main()
{
    BOOST_TEST_EQ(Bouncingball::bouncingBall(3, 0.66, 1.5), 3);
    BOOST_TEST_EQ(Bouncingball::bouncingBall(30, 0.66, 1.5), 15);
    BOOST_TEST_EQ(Bouncingball::bouncingBall(3, 1, 1.5), -1);
    return boost::report_errors();
}
