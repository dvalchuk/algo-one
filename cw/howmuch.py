import unittest

def howmuch(m, n):
    result = []
    mn = min(m, n)
    mx = max(m, n)
    for f in xrange(mn, mx + 1):
        b = ((f - 2) / 7) if ((f - 2) % 7 == 0) else 0
        if b <= 0:
            continue
        c = ((f - 1) / 9) if ((f - 1) % 9 == 0) else 0
        if c <= 0:
            continue
        result.append(["M: %d" %f, "B: %d" %b, "C: %d" %c])
    return result
    

class SolutionTester(unittest.TestCase):

    def test_one(self):
        ethalon = [["M: 37", "B: 5", "C: 4"], ["M: 100", "B: 14", "C: 11"]]
        actual = howmuch(1, 100)
        self.assertEqual(actual, ethalon)
        
        ethalon = [["M: 1045", "B: 149", "C: 116"]]
        actual = howmuch(1000, 1100)
        self.assertEqual(actual, ethalon)
        
        ethalon = [["M: 9991", "B: 1427", "C: 1110"]]
        actual = howmuch(10000, 9950)
        self.assertEqual(actual, ethalon)
        
        ethalon = [["M: 37", "B: 5", "C: 4"], ["M: 100", "B: 14", "C: 11"], ["M: 163", "B: 23", "C: 18"]]
        actual = howmuch(0, 200)
        self.assertEqual(actual, ethalon)
        
        ethalon = []
        actual = howmuch(2950, 2950)
        self.assertEqual(actual, ethalon)
        
        ethalon = [["M: 20008", "B: 2858", "C: 2223"], ["M: 20071", "B: 2867", "C: 2230"]]
        actual = howmuch(20000, 20100)
        self.assertEqual(actual, ethalon)


if __name__ == "__main__":
    unittest.main()
