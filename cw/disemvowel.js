const assert = require('assert');

function disemvowel(str)
{
    var isVowel = function (c)
    {
        return ['a', 'e', 'i', 'o', 'u'].indexOf(c.toLowerCase()) !== -1
    }
    
    var result = ""
    for (var i = 0; i < str.length; i++)
    {
        var c = str.charAt(i)
        if (!isVowel(c))
        {
            result += c
        }
    }
    
    return result;
}

try
{
    var expected = "Ths wbst s fr lsrs LL!"
    var actual = disemvowel("This website is for losers LOL!")
    assert.deepEqual(actual, expected)

    console.log('Ok')
}
catch (e)
{
    console.log(e)
}
