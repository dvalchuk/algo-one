import unittest

def disemvowel(string):
    is_vowel = lambda c: c.lower() in "aeiou"
    result = ''
    for c in string:
        if not is_vowel(c):
            result += c
    return result

class SolutionTester(unittest.TestCase):

    def test_one(self):
        expected = "Ths wbst s fr lsrs LL!"
        actual = disemvowel("This website is for losers LOL!")
        self.assertEqual(actual, expected)


if __name__ == "__main__":
    unittest.main()
