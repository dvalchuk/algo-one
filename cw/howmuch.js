const assert = require('assert');

function howmuch(m, n) {
    var result = new Array()
    var mn = Math.min(m, n)
    var mx = Math.max(m, n)
    for (var f = mn; f <= mx; ++f)
    {
        var b = ((f - 2) % 7 === 0) ? ((f - 2) / 7) : 0;
        if (b <= 0)
        {
            continue;
        }
        var c = ((f - 1) % 9 === 0) ? ((f - 1) / 9) : 0;
        if (c <= 0)
        {
            continue;
        }
        result.push(["M: " + f, "B: " + b, "C: " + c])
    }
    return result
}

try
{
    ethalon = [["M: 37", "B: 5", "C: 4"], ["M: 100", "B: 14", "C: 11"]]
    actual = howmuch(1, 100)
    assert.deepEqual(actual, ethalon)

    ethalon = [["M: 1045", "B: 149", "C: 116"]]
    actual = howmuch(1000, 1100)
    assert.deepEqual(actual, ethalon)
    
    ethalon = [["M: 9991", "B: 1427", "C: 1110"]]
    actual = howmuch(10000, 9950)
    assert.deepEqual(actual, ethalon)
    
    ethalon = [["M: 37", "B: 5", "C: 4"], ["M: 100", "B: 14", "C: 11"], ["M: 163", "B: 23", "C: 18"]]
    actual = howmuch(0, 200)
    assert.deepEqual(actual, ethalon)
    
    ethalon = []
    actual = howmuch(2950, 2950)
    assert.deepEqual(actual, ethalon)
    
    ethalon = [["M: 20008", "B: 2858", "C: 2223"], ["M: 20071", "B: 2867", "C: 2230"]]
    actual = howmuch(20000, 20100)
    assert.deepEqual(actual, ethalon)    

    console.log('Ok')
}
catch (e)
{
    console.log(e)
}
