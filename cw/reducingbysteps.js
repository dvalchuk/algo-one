const assert = require('assert')

function gcdi(x, y)
{
    return gcd(Math.abs(x), Math.abs(y))
}

function lcmu(a, b)
{
    return Math.abs(a * b) / gcdi(a, b)
}

function som(a, b)
{
    return a + b
}

let maxi = Math.max
let mini = Math.min

function operArray(fct, arr, init)
{
    let result = []
    for(let i = 0; i < arr.length; ++i)
    {
        init = fct(init, arr[i])
        result.push(init)
    }
    return result
}

function gcd(a, b)
{
    if (b)
    {
        return gcd(b, a % b);
    }
    else
    {
        return a;
    }
}

try
{
    var a = [ 18, 69, -90, -78, 65, 40 ];

    var r = [ 18, 3, 3, 3, 1, 1 ];
    var op = operArray(gcdi, a, a[0]);
    assert.deepEqual(op, r);
    r = [ 18, 414, 2070, 26910, 26910, 107640 ];
    op = operArray(lcmu, a, a[0]);
    assert.deepEqual(op, r);
    r = [ 18, 87, -3, -81, -16, 24 ];
    op = operArray(som, a, 0);
    assert.deepEqual(op, r);
    r = [ 18, 18, -90, -90, -90, -90 ];
    op = operArray(mini, a, a[0]);
    assert.deepEqual(op, r);
    r = [ 18, 69, 69, 69, 69, 69 ];
    op = operArray(maxi, a, a[0]);
    assert.deepEqual(op, r);

    console.log('Ok')
}
catch (e)
{
    console.log(e)
}
