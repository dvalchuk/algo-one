#include <string>
#include <cstdint>
#include <vector>
#include <limits>
#include <algorithm>

#include <boost/detail/lightweight_test.hpp>

namespace {

enum class ReadState : uint8_t
{
    READHH,
    READMM,
    READSS,
    READSPACE
};

typedef uint32_t Time;
typedef std::vector<Time> Times;

class Collector
{
    Time _max = std::numeric_limits<Time>::min();
    Time _min = std::numeric_limits<Time>::max();
    Time _acc = 0;
    mutable Times _all;
public:
    void push_back(const Time& t)
    {
        if (t < _min)
        {
            _min = t;
        }
        if (t > _max)
        {
            _max = t;
        }
        _acc += t;
        _all.push_back(t);
    }

    Time range() const { return _max - _min; }
    Time mean() const { return _acc / _all.size(); }
    Time median() const
    {
        const size_t center = _all.size() / 2;
        std::sort(_all.begin(), _all.end());
        return (_all.size() % 2) ? _all[center] : (_all[center - 1] + _all[center]) / 2;
    }
};

Collector parse(const std::string& in)
{
    Collector result;
    Time curtime = 0; // in seconds
    bool err = false;
    ReadState rdState = ReadState::READHH;
    std::string current;
    for(char c: in)
    {
        switch (rdState)
        {
        case ReadState::READHH:
        {
            if (std::isdigit(c))
            {
                current += c;
            }
            else
            {
                if (c == '|')
                {
                    curtime += static_cast<Time>(std::stoi(current)) * 60 * 60;
                    current = "";
                    rdState = ReadState::READMM;
                }
                else
                {
                    err = true;
                }
            }
            break;
        }
        case ReadState::READMM:
        {
            if (std::isdigit(c))
            {
                current += c;
            }
            else
            {
                if (c == '|')
                {
                    curtime += static_cast<Time>(std::stoi(current)) * 60;
                    current = "";
                    rdState = ReadState::READSS;
                }
                else
                {
                    err = true;
                }
            }
            break;
        }
        case ReadState::READSS:
        {
            if (std::isdigit(c))
            {
                current += c;
            }
            else
            {
                if (c == ',')
                {
                    curtime += static_cast<Time>(std::stoi(current));
                    current = "";
                    result.push_back(curtime);
                    curtime = 0;
                    rdState = ReadState::READSPACE;
                    
                }
                else
                {
                    err = true;
                }
            }
            break;
        }
        case ReadState::READSPACE:
        {
            if (c == ' ')
            {
                rdState = ReadState::READHH;
            }
            else
            {
                err = true;
            }
            break;
        }
        }
        if (err)
        {
            printf("parsing error\n");
            break;
        }
    }
    if (rdState == ReadState::READSS && !current.empty())
    {
        curtime += static_cast<Time>(std::stoi(current));
        result.push_back(curtime);
    }
    return result;
}

struct ChunkedTime
{
    static ChunkedTime fromSeconds(const Time& s)
    {
        ChunkedTime result;
        result.h = s / (60 * 60);
        const Time mp = s - result.h * 60 * 60;
        result.m = mp / 60;
        result.s = mp - result.m * 60;
        return result;
    }
    Time h;
    Time m;
    Time s;
};

} // namespace


class Stat
{
public:
    static std::string stat(const std::string& in);
};

std::string Stat::stat(const std::string& in)
{
    if (in.empty())
    {
        return std::string();
    }

    Collector cl = parse(in);
    char result[51];
    const ChunkedTime range = ChunkedTime::fromSeconds(cl.range());
    const ChunkedTime mean = ChunkedTime::fromSeconds(cl.mean());
    const ChunkedTime median = ChunkedTime::fromSeconds(cl.median());
    std::sprintf(result, "Range: %02d|%02d|%02d Average: %02d|%02d|%02d Median: %02d|%02d|%02d",
                 range.h, range.m, range.s,
                 mean.h, mean.m, mean.s,
                 median.h, median.m, median.s);
    return std::string(result);
}

int main()
{
    {
        const std::string ethalon = "Range: 00|47|18 Average: 01|35|15 Median: 01|32|34";
        const std::string actual = Stat::stat("01|15|59, 1|47|6, 01|17|20, 1|32|34, 2|3|17");
        BOOST_TEST_EQ(actual, ethalon);
    }
    {
        const std::string ethalon = "Range: 00|31|17 Average: 02|26|18 Median: 02|22|00";
        const std::string actual = Stat::stat("02|15|59, 2|47|16, 02|17|20, 2|32|34, 2|17|17, 2|22|00, 2|31|41");
        BOOST_TEST_EQ(ethalon, actual);
    }
    {
        const std::string ethalon = "";
        const std::string actual = Stat::stat("");
        BOOST_TEST_EQ(ethalon, actual);
    }

    return boost::report_errors();
}
