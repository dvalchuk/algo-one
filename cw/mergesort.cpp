#include <vector>
#include <iterator>
#include <ostream>
#include <algorithm>
#include <list>
#include <ctime>

#include <boost/detail/lightweight_test.hpp>
#include <iostream>

namespace {

using Data = std::vector<int>;

// for BOOST_TEST_EQ
std::ostream& operator << (std::ostream& os, const Data& in)
{
    os << '[';
    for(const auto& i: in)
    {
        os << i << ", ";
    }
    os << ']';
    return os;
}

std::ostream& operator << (std::ostream& os, const std::list<int>& in)
{
    os << '[';
    for(const auto& i: in)
    {
        os << i << ", ";
    }
    os << ']';
    return os;
}

template<class ForwardIt>
void merge(ForwardIt it1, ForwardIt et1, ForwardIt it2, ForwardIt et2)
{
    std::vector<typename ForwardIt::value_type> result(std::distance(it1, et1) + std::distance(it2, et2));
    for(auto i1 = it1, i2 = it2, ir = result.begin(); ; )
    {
        if (i1 == et1 && i2 == et2)
        {
            break;
        }
        if (i1 == et1)
        {
            *ir++ = *i2++;
            continue;
        }
        if (i2 == et2)
        {
            *ir++ = *i1++;
            continue;
        }
        if (*i2 < *i1)
        {
            *ir++ = *i2++;
        }
        else
        {
            *ir++ = *i1++;
        }
    }
    for(auto i = it1, ir = result.begin(); i != et2; ++i, ++ir)
    {
        *i = *ir;
    }
}

template<class ForwardIt>
void msort(ForwardIt it, ForwardIt et, std::forward_iterator_tag tag)
{
    const size_t sz = std::distance(it, et);
    if (sz < 2)
    {
        return;
    }
    if (sz == 2)
    {
        const auto next = std::next(it);
        if (*next < *it)
        {
            std::swap(*next, *it);
        }
        return;
    }
    const auto mid = std::next(it, sz / 2);
    msort(it, mid, tag); // exclusive
    msort(mid, et, tag);
    merge(it, mid, mid, et);
}

template<class ForwardIt>
void msort(ForwardIt it, ForwardIt et)
{
    msort(it, et, typename std::iterator_traits<ForwardIt>::iterator_category());
}

} // unnamed namespace

int main()
{
    {
        const Data ethalon;
        Data actual;
        msort(actual.begin(), actual.end());
        BOOST_TEST_EQ(ethalon, actual);
    }
    {
        const Data ethalon = {3};
        Data actual = {3};
        msort(actual.begin(), actual.end());
        BOOST_TEST_EQ(ethalon, actual);
    }
    {
        const Data ethalon = {3, 4};
        Data actual = {3, 4};
        msort(actual.begin(), actual.end());
        BOOST_TEST_EQ(ethalon, actual);
    }
    {
        const Data ethalon = {3, 4};
        Data actual = {4, 3};
        msort(actual.begin(), actual.end());
        BOOST_TEST_EQ(ethalon, actual);
    }
    {
        const Data ethalon = {3, 9, 10, 27, 38, 43, 82};
        Data actual = {38, 27, 43, 3, 9, 82, 10};
        msort(actual.begin(), actual.end());
        BOOST_TEST_EQ(ethalon, actual);
    }
    {
        std::srand(std::time(nullptr));
        auto rnd = [] (size_t r) { return std::rand() % r; };
        const size_t itemsNum = rnd(10000);
        Data ethalon(itemsNum);
        std::generate(ethalon.begin(), ethalon.end(), [&itemsNum, &rnd] { return rnd(itemsNum * 2); });
        Data actual(ethalon);
        std::sort(ethalon.begin(), ethalon.end());
        msort(actual.begin(), actual.end());
        BOOST_TEST_EQ(ethalon, actual);
    }
    {
        const std::list<int> ethalon = {3, 9, 10, 27, 38, 43, 82};
        std::list<int> actual = {38, 27, 43, 3, 9, 82, 10};
        msort(actual.begin(), actual.end());
        BOOST_TEST_EQ(ethalon, actual);
    }
    
    return boost::report_errors(); 
}
