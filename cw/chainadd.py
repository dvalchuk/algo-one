import unittest

class add(int):
    def __call__(self, n):
        return add(self + n)


class SolutionTester(unittest.TestCase):

    def test_task(self):
        self.assertEqual(add(1), 1)
        self.assertEqual(add(1)(2), 3)
        self.assertEqual(add(1)(2)(3), 6)

        addTwo = add(2);
        self.assertEqual(addTwo, 2);
        self.assertEqual(addTwo + 5, 7);
        self.assertEqual(addTwo - 1, 1);
        self.assertEqual(addTwo(3), 5);    
        self.assertEqual(addTwo(3)(5), 10);

if __name__ == "__main__":
    unittest.main()
