const assert = require('assert');

function duplicateCount(s)
{
    var result = 0
    var all = new Array('\uFFFF'.charCodeAt(0)).fill(0)
    for (var i = 0; i < s.length; i++)
    {
        var cCode = s.charAt(i).toLowerCase().charCodeAt(0)
        if (all[cCode] == 1)
        {
            result++
        }
        all[cCode]++
    }
    return result
}

try
{
    assert.deepEqual(duplicateCount(' '), 0)
    assert.deepEqual(duplicateCount(''), 0)
    assert.deepEqual(duplicateCount("asdfghjkl;'\\"), 0)
    assert.deepEqual(duplicateCount("asdfghjkl;'\\'"), 1)
    assert.deepEqual(duplicateCount("asdfghjkl;'\\qwertyuiop[]{}:\"|zxcvbnm,./<>?1234567890-=`~!@#$%^&*()_+ "), 0)
    assert.deepEqual(duplicateCount("abcde"), 0)
    assert.deepEqual(duplicateCount("aabbcde"), 2)
    assert.deepEqual(duplicateCount("aabBcde"), 2,"should ignore case")
    assert.deepEqual(duplicateCount("Indivisibility"), 1)
    assert.deepEqual(duplicateCount("Indivisibilities"), 2)
    console.log('Ok')
}
catch (e)
{
    console.log(e)
}
