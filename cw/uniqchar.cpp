#include <limits>
#include <vector>
#include <cstdint>
#include <algorithm>

#include <boost/detail/lightweight_test.hpp> 

namespace {

bool uniq(const char* in)
{
    static std::vector<bool> all(std::numeric_limits<unsigned char>::max()/* + 1*/, false); // one char for '\0' is not used
    std::fill(all.begin(), all.end(), false);
    for (const char* c = in; *c != '\0'; ++c)
    {
        std::vector<bool>::reference v = all[static_cast<int>(*c)];
        if (v)
            return false;
        v = true;
    }
    return true;
}

}

int main()
{
    BOOST_TEST_EQ(uniq(" "), true);
    BOOST_TEST_EQ(uniq(""), true);
    BOOST_TEST_EQ(uniq("asdfghjkl;'\\"), true);
    BOOST_TEST_EQ(uniq("asdfghjkl;'\\'"), false);
    BOOST_TEST_EQ(uniq("asdfghjkl;'\\qwertyuiop[]{}:\"|zxcvbnm,./<>?1234567890-=`~!@#$%^&*()_+ "), true);
    return boost::report_errors();
}
