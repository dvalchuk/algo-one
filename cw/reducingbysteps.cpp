#include <vector>
#include <algorithm>
#include <cmath>

#include <boost/detail/lightweight_test.hpp>
#include <ostream>
#include <iostream>

namespace {
typedef std::vector<long long> Data;
long long gcd(long long a, long long b);

std::ostream& operator << (std::ostream& ss, const Data& dd);
} // namespace 

class Operarray
{
public:
    static long long gcdi(long long x, long long y);
    static long long lcmu(long long a, long long b);
    static long long som(long long a, long long b);
    static long long maxi(long long a, long long b);
    static long long mini(long long a, long long b);
    template<typename Func>
    static ::Data operArray(Func f, Data &arr, long long init);
};

long long Operarray::gcdi(long long x, long long y)
{
    return gcd(std::abs(x), std::abs(y));
}

long long Operarray::lcmu(long long a, long long b)
{
    return std::abs(a * b) / gcdi(a, b);
}

long long Operarray::som(long long a, long long b)
{
    return a + b;
}

long long Operarray::maxi(long long a, long long b)
{
    return std::max(a, b);
}

long long Operarray::mini(long long a, long long b)
{
    return std::min(a, b);
}

template<typename Func>
::Data Operarray::operArray(Func f, Data &arr, long long init)
{
    const size_t sz = arr.size();
    Data result(sz);
    for(size_t i = 0; i < sz; ++i)
    {
        init = f(init, arr[i]);
        result[i] = init;
    }
    return result;
}

namespace {

long long gcd(long long a, long long b)
{
    if (a == b)
        return a;

    if (a == 0)
        return b;

    if (b == 0)
        return a;

    // look for factors of 2
    if (~a & 1) // a is even
    {
        if (b & 1) // b is odd
            return gcd(a >> 1, b);
        else // both a and b are even
            return gcd(a >> 1, b >> 1) << 1;
    }

    if (~b & 1) // a is odd, b is even
        return gcd(a, b >> 1);

    // reduce larger argument
    if (a > b)
        return gcd((a - b) >> 1, b);

    return gcd((b - a) >> 1, a);
}

std::ostream& operator << (std::ostream& ss, const Data& dd)
{
    ss << "[";
    for(const auto& d: dd)
    {
        ss << d << ", ";
    }
    ss << "]";
    return ss;
}

void testequal(Data ans, Data sol)
{
    BOOST_TEST_EQ(ans, sol);
}

template<typename Func>
static void dotest(Func func, Data &arr, long long init, Data expected)
{
    testequal(Operarray::operArray(func, arr, init), expected);
}

} // unnamed

int main()
{
    Data dta = { 18, 69, -90, -78, 65, 40 };
    
    Data sol = { 18, 3, 3, 3, 1, 1 };
    dotest(Operarray::gcdi, dta, dta[0], sol);
    
    sol = { 18, 414, 2070, 26910, 26910, 107640 };
    dotest(Operarray::lcmu, dta, dta[0], sol);
    
    sol = { 18, 69, 69, 69, 69, 69 };
    dotest(Operarray::maxi, dta, dta[0], sol);

    sol = { 18, 18, -90, -90, -90, -90 };
    dotest(Operarray::mini, dta, dta[0], sol);

    sol = { 18, 87, -3, -81, -16, 24 };
    dotest(Operarray::som, dta, 0, sol);

    return boost::report_errors();
}
