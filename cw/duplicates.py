import unittest
import sys

def duplicate_count(s):
    result = 0
    all = [False] * sys.maxunicode
    for c in s:
        i = ord(c.lower())
        if all[i] == 1:
            result += 1
        all[i] += 1
    return result

class SolutionTester(unittest.TestCase):

    def test_task(self):
        self.assertEqual(duplicate_count(' '), 0)
        self.assertEqual(duplicate_count(''), 0)
        self.assertEqual(duplicate_count("asdfghjkl;'\\"), 0)
        self.assertEqual(duplicate_count("asdfghjkl;'\\'"), 1)
        self.assertEqual(duplicate_count("asdfghjkl;'\\qwertyuiop[]{}:\"|zxcvbnm,./<>?1234567890-=`~!@#$%^&*()_+ "), 0)
        self.assertEqual(duplicate_count("abcde"), 0)
        self.assertEqual(duplicate_count("aabbcde"), 2)
        self.assertEqual(duplicate_count("aabBcde"), 2,"should ignore case")
        self.assertEqual(duplicate_count("Indivisibility"), 1)
        self.assertEqual(duplicate_count("Indivisibilities"), 2)

if __name__ == "__main__":
    unittest.main()
