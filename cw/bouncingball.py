import unittest

def bouncingBall(h, bounce, window):
    if bounce <= 0 or bounce >= 1.0 or h <= 0 or window >= h:
        return -1
    result = 1 # down
    remaining = h * bounce
    if remaining >= window:
        result += 1 # up
        return result + bouncingBall(remaining, bounce, window)
    return result

class SolutionTester(unittest.TestCase):

    def test_one(self):
        self.assertEqual(bouncingBall(3, 0.66, 1.5), 3);
        self.assertEqual(bouncingBall(30, 0.66, 1.5), 15);
        self.assertEqual(bouncingBall(3, 1, 1.5), -1);


if __name__ == "__main__":
    unittest.main()
