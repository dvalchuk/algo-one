#include <string>
#include <sstream>
#include <cassert>

#include <boost/detail/lightweight_test.hpp>

class Carboat
{
public:
    static std::string howmuch(int m, int n);
};

std::string Carboat::howmuch(int m, int n)
{
    assert(m >= 0);
    assert(n >= 0);
    std::stringstream result;
    result << '[';
    const int min = std::min(m, n);
    const int max = std::max(m, n);
    for (int f = min; f <= max; ++f)
    {
        const int b = ((f - 2) % 7 == 0) ? ((f - 2) / 7) : 0;
        if (b <= 0)
        {
            continue;
        }
        const int c = ((f - 1) % 9 == 0) ? ((f - 1) / 9) : 0;
        if (c <= 0)
        {
            continue;
        }
        result << "[M: " << f << " B: " << b << " C: " << c << "]";
    }
    result << ']';
    return result.str();
}

int main()
{
    {
        const std::string ethalon = "[[M: 37 B: 5 C: 4][M: 100 B: 14 C: 11]]";
        const std::string actual = Carboat::howmuch(1, 100);
        BOOST_TEST_EQ(actual, ethalon);
    }
    {
        const std::string ethalon = "[[M: 1045 B: 149 C: 116]]";
        const std::string actual = Carboat::howmuch(1000, 1100);
        BOOST_TEST_EQ(actual, ethalon);
    }
    {
        const std::string ethalon = "[[M: 9991 B: 1427 C: 1110]]";
        const std::string actual = Carboat::howmuch(10000, 9950);
        BOOST_TEST_EQ(actual, ethalon);
    }
    {
        const std::string ethalon = "[[M: 37 B: 5 C: 4][M: 100 B: 14 C: 11][M: 163 B: 23 C: 18]]";
        const std::string actual = Carboat::howmuch(0, 200);
        BOOST_TEST_EQ(actual, ethalon);
    }
    {
        const std::string ethalon = "[]";
        const std::string actual = Carboat::howmuch(2950, 2950);
        BOOST_TEST_EQ(actual, ethalon);
    }
    {
        const std::string ethalon = "[[M: 20008 B: 2858 C: 2223][M: 20071 B: 2867 C: 2230]]";
        const std::string actual = Carboat::howmuch(20000, 20100);
        BOOST_TEST_EQ(actual, ethalon);
    }
    return boost::report_errors();
}
