import sys
import re

import unittest

class Collector(object):

    def __init__(self):
        self.all = []
        self.min = sys.maxint
        self.max = 0
        self.acc = 0

    def add(self, t):
        if t < self.min:
            self.min = t
        if t > self.max:
            self.max = t
        self.acc += t
        self.all.append(t)

    def range(self):
        return self.max - self.min

    def mean(self):
        return self.acc / len(self.all)

    def median(self):
        center = len(self.all) / 2
        self.all.sort()
        return self.all[center] if (len(self.all) % 2) else (self.all[center - 1] + self.all[center]) / 2

def fromSeconds(s):
    h = s / (60 * 60)
    mp = s - h * 60 * 60
    m = mp / 60
    s = mp - m * 60
    return (h, m, s)
    

def stat(sin):
    if not sin:
        return ''
    cl = Collector()
    rx = '(([0-9]{1,2})\\|([0-9]{1,2})\\|([0-9]{1,2})[, ]{0,1})'
    for m in re.finditer(rx, sin):
        (h, m, s) = m.group(2, 3, 4)
        ss = int(h) * 60 * 60
        ss += int(m) * 60
        ss += int(s)
        cl.add(ss)
    range = fromSeconds(cl.range())
    mean = fromSeconds(cl.mean())
    median = fromSeconds(cl.median())
    return 'Range: {:02d}|{:02d}|{:02d} Average: {:02d}|{:02d}|{:02d} Median: {:02d}|{:02d}|{:02d}'.format(
        range[0], range[1], range[2],
        mean[0], mean[1], mean[2],
        median[0], median[1], median[2]
    )


class SolutionTester(unittest.TestCase):

    def test_task(self):
        self.assertEqual(stat("01|15|59, 1|47|16, 01|17|20, 1|32|34, 2|17|17"),
                               "Range: 01|01|18 Average: 01|38|05 Median: 01|32|34")
        self.assertEqual(stat("02|15|59, 2|47|16, 02|17|20, 2|32|34, 2|17|17, 2|22|00, 2|31|41"),
                               "Range: 00|31|17 Average: 02|26|18 Median: 02|22|00")
        self.assertEqual(stat(""), "")


if __name__ == "__main__":
    unittest.main()
