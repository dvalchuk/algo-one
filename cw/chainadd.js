const assert = require('assert')

class Adder extends Function {
    constructor(x)
    {
        let fo = Object.setPrototypeOf(function(n) { return new Adder(x + n) }, Adder.prototype)
        fo.x = x
        return fo
    }
}

Adder.prototype.valueOf = function() {
    return this.x
}

function add(n) {
    return new Adder(n)
}

try
{
    assert.deepEqual(add(1), 1)
    assert.deepEqual(add(1)(2), 3)
    assert.deepEqual(add(1)(2)(3), 6)

    let addTwo = add(2);
    assert.deepEqual(addTwo, 2);
    assert.deepEqual(addTwo + 5, 7);
    assert.deepEqual(addTwo - 1, 1);
    assert.deepEqual(addTwo(3), 5);    
    assert.deepEqual(addTwo(3)(5), 10);

    console.log('Ok')
}
catch (e)
{
    console.log(e)
}
