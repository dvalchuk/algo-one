#include <vector>
#include <algorithm>

#include <boost/detail/lightweight_test.hpp> 

int queueTime(std::vector<int> customers,int n)
{
    std::vector<int> tills(n);
    for(auto customer: customers)
    {
        auto min = std::min_element(begin(tills), end(tills));
        *min += customer;
    }
    return *std::max_element(begin(tills), end(tills));
}

int main(int, const char**)
{
    BOOST_TEST_EQ(queueTime(std::vector<int>{}, 1), 0);
    BOOST_TEST_EQ(queueTime(std::vector<int>{1,2,3,4}, 1), 10);
    BOOST_TEST_EQ(queueTime(std::vector<int>{2,2,3,3,4,4}, 2), 9);
    BOOST_TEST_EQ(queueTime(std::vector<int>{1,2,3,4,5}, 100), 5);
    BOOST_TEST_EQ(queueTime(std::vector<int>{2,3,10}, 2), 12);
    BOOST_TEST_EQ(queueTime(std::vector<int>{10,2,3,3}, 2), 10);
    BOOST_TEST_EQ(queueTime(std::vector<int>{5, 2}, 1), 7);
    return boost::report_errors();
}
