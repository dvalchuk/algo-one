import unittest

def in_arr2(s1, arr2):
    for s2 in arr2:
        if s1 in s2:
            return True
    return False

def in_array(array1, array2):
    return sorted({s1 for s1 in array1 if in_arr2(s1, array2)})

class SolutionTester(unittest.TestCase):

    def test_task(self):
        a1 = ["live", "arp", "strong"]
        a2 = ["lively", "alive", "harp", "sharp", "armstrong"]
        ethalon = ['arp', 'live', 'strong']
        actual = in_array(a1, a2)
        self.assertEqual(actual, ethalon)

        a1 = ["tarp", "mice", "bull"]
        a2 = ["lively", "alive", "harp", "sharp", "armstrong"]
        ethalon = []
        actual = in_array(a1, a2)
        self.assertEqual(actual, ethalon)

        a1 = ['duplicates', 'duplicates']
        a2 = ['duplicates']
        ethalon = ['duplicates']
        actual = in_array(a1, a2)
        self.assertEqual(actual, ethalon)

if __name__ == "__main__":
    unittest.main()
