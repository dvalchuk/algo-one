import unittest

import random
import copy


def merge(arr, it1, et1, it2, et2):
    result = [None] * (et1 - it1 + et2 - it2)
    i1 = it1
    i2 = it2
    ir = 0
    while True:
        if i1 == et1 and i2 == et2:
            break
        if i1 == et1:
            result[ir] = arr[i2]
            ir += 1
            i2 += 1
            continue
        if i2 == et2:
            result[ir] = arr[i1]
            ir += 1
            i1 += 1
            continue
        if arr[i2] < arr[i1]:
            result[ir] = arr[i2]
            i2 += 1
        else:
            result[ir] = arr[i1]
            i1 += 1
        ir += 1
    ir = 0
    for i in xrange(it1, et2):
        arr[i] = result[ir]
        ir += 1

def msort(arr, it = 0, et = None):
    if et is None:
        et = len(arr)
    sz = et - it
#    print(arr, it, et, sz)
    if sz < 2:
        return
    if sz == 2:
        if arr[it + 1] < arr[it]:
            (arr[it + 1], arr[it]) = (arr[it], arr[it + 1])
        return
    mid = it + sz / 2
    msort(arr, it, mid)
    msort(arr, mid, et)
    merge(arr, it, mid, mid, et)


class SolutionTester(unittest.TestCase):

    def test_task(self):
        ethalon = []
        actual = []
        msort(actual)
        self.assertEqual(ethalon, actual);
        
        ethalon = [3]
        actual = [3]
        msort(actual)
        self.assertEqual(ethalon, actual);
        
        ethalon = [3, 4]
        actual = [3, 4]
        msort(actual)
        self.assertEqual(ethalon, actual);
        
        ethalon = [3, 4]
        actual = [4, 3]
        msort(actual)
        self.assertEqual(ethalon, actual);
        
        ethalon = [3, 9, 10, 27, 38, 43, 82]
        actual = [38, 27, 43, 3, 9, 82, 10]
        msort(actual)
        self.assertEqual(ethalon, actual);

        random.seed()
        itemsNum = random.randrange(10000)
        ethalon = [random.randrange(itemsNum * 2) for i in xrange(itemsNum)]
        actual = copy.copy(ethalon)
        ethalon.sort()
        msort(actual)
        self.assertEqual(ethalon, actual)

if __name__ == "__main__":
    unittest.main()
