const assert = require('assert')

function bouncingBall(h,  bounce,  window)
{
    if (bounce <= 0 || bounce >= 1.0 || h <= 0 || window >= h)
    {
        return -1
    }
    var result = 1 // down
    var remaining = h * bounce
    if (remaining >= window)
    {
        ++result // up
        return result + bouncingBall(remaining, bounce, window)
    }
    return result
}

try
{
    assert.deepEqual(bouncingBall(3, 0.66, 1.5), 3)
    assert.deepEqual(bouncingBall(30, 0.66, 1.5), 15)
    assert.deepEqual(bouncingBall(3, 1, 1.5), -1)

    console.log('Ok')
}
catch (e)
{
    console.log(e)
}
