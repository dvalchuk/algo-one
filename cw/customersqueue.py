import unittest


def queue_time(customers, n):
    tills = [0] * n
    for t in customers:
        minIdx = min(range(n), key = tills.__getitem__)
        tills[minIdx] += t
    return max(tills)


class SolutionTester(unittest.TestCase):

    def test_queue_time(self):
        self.assertEqual(queue_time([5], 1), 5)
        self.assertEqual(queue_time([], 1), 0);
        self.assertEqual(queue_time([1,2,3,4], 1), 10);
        self.assertEqual(queue_time([2,2,3,3,4,4], 2), 9);
        self.assertEqual(queue_time([1,2,3,4,5], 100), 5);
        self.assertEqual(queue_time([2,3,10], 2), 12);
        self.assertEqual(queue_time([10,2,3,3], 2), 10);
        self.assertEqual(queue_time([5, 2], 1), 7);

if __name__ == "__main__":
    unittest.main()
