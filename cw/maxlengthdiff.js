const assert = require('assert');

try
{

    function mxdiflg(a1, a2)
    {
        if (a1.length == 0 || a2.length == 0)
            return -1;
        var cmp = (lhv, rhv) => lhv.length - rhv.length
        a1.sort(cmp)
        a2.sort(cmp)
        return Math.max(a1[a1.length - 1].length - a2[0].length, a2[a2.length - 1].length - a1[0].length)
    }

    a1 = ["hoqq", "bbllkw", "oox", "ejjuyyy", "plmiis", "xxxzgpsssa", "xxwwkktt", "znnnnfqknaz", "qqquuhii", "dvvvwz"]
    a2 = ["cccooommaaqqoxii", "gggqaffhhh", "tttoowwwmmww"]
    assert.deepEqual(mxdiflg(a1, a2), 13)
    a1 = ["ejjjjmmtthh", "zxxuueeg", "aanlljrrrxx", "dqqqaaabbb", "oocccffuucccjjjkkkjyyyeehh"]
    a2 = ["bbbaaayddqbbrrrv"]
    assert.deepEqual(mxdiflg(a1, a2), 10)

    console.log('Ok')
}
catch (e)
{
    console.log(e)
}
