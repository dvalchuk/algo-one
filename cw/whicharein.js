const assert = require('assert')

function inArray(array1, array2)
{
    var result = new Set()
    for(var i = 0; i < array1.length; ++i)
    {
        var s1 = array1[i]
        var found = array2.find(function(s2) { return s2.includes(s1) })
        if (found)
        {
            result.add(s1)
            continue
        }
    }
    return Array.from(result).sort()
}

try
{
    a1 = ["live", "arp", "strong"]
    a2 = ["lively", "alive", "harp", "sharp", "armstrong"]
    ethalon = ['arp', 'live', 'strong']
    actual = inArray(a1, a2)
    assert.deepEqual(actual, ethalon)

    a1 = ["tarp", "mice", "bull"]
    a2 = ["lively", "alive", "harp", "sharp", "armstrong"]
    ethalon = []
    actual = inArray(a1, a2)
    assert.deepEqual(actual, ethalon)

    a1 = ['duplicates', 'duplicates']
    a2 = ['duplicates']
    ethalon = ['duplicates']
    actual = inArray(a1, a2)
    assert.deepEqual(actual, ethalon)

    console.log('Ok')
}
catch (e)
{
    console.log(e)
}
