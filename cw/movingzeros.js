const assert = require('assert')

var moveZeros = function (arr)
{
    let result = arr.filter((a) => a !== 0)
    return result.concat(new Array(arr.length - result.length).fill(0))
}

try
{
    assert.deepEqual(moveZeros([1,2,0,1,0,1,0,3,0,1]), [ 1, 2, 1, 1, 3, 1, 0, 0, 0, 0 ])
    assert.deepEqual(moveZeros([9,0.0,0,9,1,2,0,1,0,1,0.0,3,0,1,9,0,0,0,0,9]), [9,9,1,2,1,1,3,1,9,9,0,0,0,0,0,0,0,0,0,0])
    assert.deepEqual(moveZeros(["a",0,0,"b","c","d",0,1,0,1,0,3,0,1,9,0,0,0,0,9]), ["a","b","c","d",1,1,3,1,9,9,0,0,0,0,0,0,0,0,0,0])
    assert.deepEqual(moveZeros(["a",0,0,"b",undefined,"c","d",0,1,false,0,1,0,3,[],0,1,9,0,0,{},0,0,9]), ["a","b",undefined,"c","d",1,false,1,3,[],1,9,{},9,0,0,0,0,0,0,0,0,0,0])
    assert.deepEqual(moveZeros([0,1,undefined,2,false,1,0]), [1,undefined,2,false,1,0,0])
    assert.deepEqual(moveZeros(["a","b"]), ["a","b"])
    assert.deepEqual(moveZeros(["a"]), ["a"])
    assert.deepEqual(moveZeros([0,0]), [0,0])
    assert.deepEqual(moveZeros([0]), [0])
    assert.deepEqual(moveZeros([]), [])

    console.log('Ok')
}
catch (e)
{
    console.log(e)
}
