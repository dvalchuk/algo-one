const assert = require('assert');

try
{

    function uniq(s)
    {
        var all = new Array('\uFFFF'.charCodeAt(0)).fill(false)
        for (var i = 0; i < s.length; i++)
        {
            var cCode = s.charCodeAt(i)
            if (all[cCode])
            {
                return false
            }
            all[cCode] = true
        }
        return true
    }

    assert.deepEqual(uniq(' '), true)
    assert.deepEqual(uniq(''), true)
    assert.deepEqual(uniq("asdfghjkl;'\\"), true)
    assert.deepEqual(uniq("asdfghjkl;'\\'"), false)
    assert.deepEqual(uniq("asdfghjkl;'\\qwertyuiop[]{}:\"|zxcvbnm,./<>?1234567890-=`~!@#$%^&*()_+ "), true)
    console.log('Ok')
}
catch (e)
{
    console.log(e)
}
