import unittest
import sys

def uniq(s):
    all = [False] * sys.maxunicode
    for c in s:
        i = ord(c)
        if all[i]:
            return False
        all[i] = True
    return True

class SolutionTester(unittest.TestCase):

    def test_uniq(self):
        self.assertEqual(uniq(' '), True)
        self.assertEqual(uniq(''), True)
        self.assertEqual(uniq("asdfghjkl;'\\"), True)
        self.assertEqual(uniq("asdfghjkl;'\\'"), False)
        self.assertEqual(uniq("asdfghjkl;'\\qwertyuiop[]{}:\"|zxcvbnm,./<>?1234567890-=`~!@#$%^&*()_+ "), True)

if __name__ == "__main__":
    unittest.main()
