import unittest

class Kata:
    def fold_array(self, array, runs):
        if len(array) == 1:
            return array
        if runs == 1:
            return self._fold(array)
        else:
            return self.fold_array(self._fold(array), runs - 1)

    def _fold(self, array):
        new_size = (len(array) + 1) / 2
        result = [0] * new_size
        for i in xrange(len(array) / 2):
            result[i] = array[i] + array[len(array) - 1 - i]
        if new_size > len(array) / 2:
            result[new_size - 1] = array[new_size - 1]
        return result

class SolutionTester(unittest.TestCase):

    def test_one(self):
        kata = Kata()
        expected = [ 6, 6, 3 ]
        actual = kata.fold_array([ 1, 2, 3, 4, 5 ], 1)
        self.assertEqual(actual, expected)

        expected = [ 9, 6 ]
        actual = kata.fold_array([ 1, 2, 3, 4, 5 ], 2)
        self.assertEqual(actual, expected)
        
        expected = [ 15 ]
        actual = kata.fold_array([ 1, 2, 3, 4, 5 ], 3)
        self.assertEqual(actual, expected)
        
        expected = [ 14, 75, 0 ]
        actual = kata.fold_array([ -9, 9, -8, 8, 66, 23 ], 1)
        self.assertEqual(actual, expected)
        
        expected = [ 15 ]
        actual = kata.fold_array([ 15 ], 3)
        self.assertEqual(actual, expected)


if __name__ == "__main__":
    unittest.main()
