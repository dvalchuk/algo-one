#include <string>
#include <vector>
#include <set>
#include <algorithm>

#include <ostream>
#include <boost/detail/lightweight_test.hpp> 

namespace {

typedef std::vector<std::string> Data;
std::ostream& operator << (std::ostream& os, const Data& d);

class WhichAreIn
{

public:
    static Data inArray(Data &array1, Data &array2);
};

Data WhichAreIn::inArray(Data &array1, Data &array2)
{
    std::set<std::string> result;
    for(const auto& s1: array1)
    {
        auto fit = std::find_if(array2.begin(), array2.end(),
                                [&s1] (const std::string& s2) {
                                    return s2.find(s1.c_str()) != std::string::npos; });
        if (fit != array2.end())
        {
            result.insert(s1);
            continue;
        }
    }
    return Data(result.begin(), result.end());
}

std::ostream& operator << (std::ostream& os, const Data& d)
{
    os << "[";
    for(const auto& i: d)
    {
        os << i << ", ";
    }
    os << "]";
    return os;
}

}

int main()
{
    {
        Data arr1 = { "arp", "live", "strong" };
        Data arr2 = { "lively", "alive", "harp", "sharp", "armstrong" };
        const Data ethalon = { "arp", "live", "strong" };
        const Data actual = WhichAreIn::inArray(arr1, arr2);
        BOOST_TEST_EQ(ethalon, actual);
    }
    {
        Data arr1 = { "tarp", "mice", "bull" };
        Data arr2 = { "lively", "alive", "harp", "sharp", "armstrong" };
        const Data ethalon = {};
        const Data actual = WhichAreIn::inArray(arr1, arr2);
        BOOST_TEST_EQ(ethalon, actual);
    }
    {
        Data arr1 = { "tarp", "tarp"};
        Data arr2 = { "bzztarp"};
        const Data ethalon = {"tarp"};
        const Data actual = WhichAreIn::inArray(arr1, arr2);
        BOOST_TEST_EQ(ethalon, actual);
    }

    return boost::report_errors();
}
