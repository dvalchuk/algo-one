#include <ostream>

#include <boost/detail/lightweight_test.hpp>
#include <iostream>

namespace {

class Adder;

std::ostream& operator << (std::ostream& os, const Adder& rhv);
Adder operator + (const Adder& lhv, int rhv);

class Adder
{
    int _n;
public:
    Adder(int n) : _n(n) {}

    Adder operator () (int rhv)
    {
        return *this + rhv;
    }
    
    operator int () const { return _n; }
};

std::ostream& operator << (std::ostream& os, const Adder& rhv)
{
    os << static_cast<int>(rhv);
    return os;
}

Adder operator + (const Adder& lhv, int rhv)
{
    return Adder(static_cast<int>(lhv) + rhv);
}

auto add(int n)
{
    return Adder(n);
}

} // unnamed namespace


int main()
{
    std::cout << "add(1) == 1\n";
    BOOST_TEST_EQ(add(1), 1);
    std::cout << "add(1)(2) == 3\n";
    BOOST_TEST_EQ(add(1)(2), 3);
    std::cout << "add(1)(2)(3) == 6\n";
    BOOST_TEST_EQ(add(1)(2)(3), 6);

    auto addTwo = add(2);
    BOOST_TEST_EQ(addTwo, 2);
    BOOST_TEST_EQ(addTwo + 5, 7);
    BOOST_TEST_EQ(addTwo(3), 5);    
    BOOST_TEST_EQ(addTwo(3)(5), 10);

    return boost::report_errors();
}
