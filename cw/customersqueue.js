const assert = require('assert');

try
{
    function queueTime(customers, n)
    {
        var tills = new Array(n).fill(0);
        for (var i = 0; i < customers.length; i++)
        {
            var minIdx = tills.reduce(function (iMin, v, i, arr) { return v < arr[iMin] ? i : iMin } , 0);
            tills[minIdx] += customers[i];
        }
        return tills.reduce(function(l, r) { return Math.max(l, r); });
    }

    assert.deepEqual(queueTime([5], 1), 5)
    assert.deepEqual(queueTime([], 1), 0);
    assert.deepEqual(queueTime([1,2,3,4], 1), 10);
    assert.deepEqual(queueTime([2,2,3,3,4,4], 2), 9);
    assert.deepEqual(queueTime([1,2,3,4,5], 100), 5);
    assert.deepEqual(queueTime([2,3,10], 2), 12);
    assert.deepEqual(queueTime([10,2,3,3], 2), 10);
    assert.deepEqual(queueTime([5, 2], 1), 7);
    console.log('Ok')
}
catch (e)
{
    console.log(e)
}
