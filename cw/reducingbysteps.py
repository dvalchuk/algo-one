import unittest
import fractions

def gcdi(a, b):
    return fractions.gcd(abs(a), abs(b))

def lcmu(a, b):
    return abs(a * b) / gcdi(a, b)

def som(a, b):
    return a + b

def maxi(a, b):
    return max(a, b)

def mini(a, b):
    return min(a, b)
    
def oper_array(fct, arr, init):
    result = []
    for a in arr:
        init = fct(init, a)
        result.append(init)
    return result


class SolutionTester(unittest.TestCase):

    def test_task(self):
        a = [ 18, 69, -90, -78, 65, 40 ]
        r = [ 18, 3, 3, 3, 1, 1 ]
        op = oper_array(gcdi, a, a[0])
        self.assertEqual(op, r)
        r = [ 18, 414, 2070, 26910, 26910, 107640 ]
        op = oper_array(lcmu, a, a[0])
        self.assertEqual(op, r)
        r = [ 18, 87, -3, -81, -16, 24 ]
        op = oper_array(som, a, 0)
        self.assertEqual(op, r)
        r = [ 18, 18, -90, -90, -90, -90 ]
        op = oper_array(mini, a, a[0])
        self.assertEqual(op, r)
        r = [ 18, 69, 69, 69, 69, 69 ]
        op = oper_array(maxi, a, a[0])
        self.assertEqual(op, r)

if __name__ == "__main__":
    unittest.main()
