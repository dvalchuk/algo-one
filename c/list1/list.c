#include <stdlib.h>
#include <stdio.h>
#include <string.h>


struct vdt_litem
{
    size_t size;
    char* data;
    struct vdt_litem* next;
};

struct vdt_list
{
    size_t size;
    struct vdt_litem* head;
    struct vdt_litem* last;
};

struct vdt_litem* vdt_litem_create(const char* data, size_t size)
{
    struct vdt_litem* result = (struct vdt_litem*) malloc(sizeof(*result));
    if (result != NULL)
    {
        result->data = (char*) malloc(size);
        memcpy(result->data, data, size);
        result->size = size;
        result->next = NULL;
        return result;
    }
    return NULL;
}

void vdt_litem_free(struct vdt_litem* item)
{
    if (item->data != NULL)
    {
        free(item->data);
    }
    free(item);
}

struct vdt_list* vdt_list_create(void)
{
    struct vdt_list* result = (struct vdt_list*) malloc(sizeof(*result));
    if (NULL != result)
    {
        result->head = NULL;
        result->last = NULL;
        result->size = 0u;
    }
    return result;
}

void vdt_list_free(struct vdt_list* list)
{
    if (list->head != NULL)
    {
        struct vdt_litem* current = list->head;
        do
        {
            struct vdt_litem* storedNext = current->next;
            vdt_litem_free(current);
            current = storedNext;
        } while (NULL != current);
    }
    free(list);
}

void vdt_list_add(struct vdt_list* list, struct vdt_litem* item)
{
    if (NULL == list->head)
    {
        list->head = item;
        list->last = item;
    }
    else 
    {
        list->last->next = item;
        list->last = item;
    }
    ++list->size;
}

void vdt_print_list(const struct vdt_list* list, size_t n) // n == 0 - print all
{
    if (NULL == list->head)
    {
        return;
    }
    struct vdt_litem* current = list->head;
    size_t i = 0;
    do
    {
        const size_t size = current->size;
        const size_t data = *(size_t*)current->data;
        printf("data: %ld size: %ld\n", data, size);
        current = current->next;
        ++i;
    } while (NULL != current && i != n);
}

void vdt_list_reverse(struct vdt_list* list)
{
    if (NULL == list->head)
    {
        return;
    }
    struct vdt_litem* current = list->head;
    struct vdt_litem* prev = NULL;
    while(NULL != current)
    {
        struct vdt_litem* storedNext = current->next;
        current->next = prev;
        prev = current;
        if (NULL != storedNext)
        {
            current = storedNext;
        }
        else
        {
            break;
        }
    }
    list->last = list->head;
    list->head = current;
}

int main(int argc, const char* argv[])
{
    struct vdt_list* list = vdt_list_create();
    if (NULL == list)
    {
        return EXIT_FAILURE;
    }
    for (size_t i = 0u; i < 500000; ++i)
    {
        struct vdt_litem* litem = vdt_litem_create((const char*)&i, sizeof(size_t)); 
        if (litem)
        {
            vdt_list_add(list, litem);
        }
    }
    vdt_print_list(list, 5);
    vdt_list_reverse(list);
    printf("-------------------\n");
    vdt_print_list(list, 5);
        
    vdt_list_free(list);
    return EXIT_SUCCESS;
}

