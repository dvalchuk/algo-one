#define BOOST_SPIRIT_DEBUG

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_object.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/predicate.hpp>

#include <cstdlib>
#include <string>
#include <vector>
#include <complex>


using namespace boost::spirit;
using std::cout;
using std::endl;


namespace client {

namespace qi = boost::spirit::qi;
namespace phoenix = boost::phoenix;
namespace ascii = boost::spirit::ascii;

}


namespace client
{

template <typename Iterator>
struct attributes_parser : qi::grammar<Iterator, std::string(), ascii::space_type>
{
    attributes_parser() : attributes_parser::base_type(values)
    {
        using namespace qi::labels;
        using qi::uint_;
        using qi::on_error;
        using qi::fail;
        using qi::debug;
        using ascii::char_;
        using qi::lexeme;

        using phoenix::construct;
        using phoenix::val;

        item %= lexeme[+(char_)];
        values %= item | item >> *(',' >> item);
        attr %= item | '(' >> item >> '=' >> values >> ')';
        attrs %= attr | attr >> *(',' >> attr);


        attrs.name("attrs");
        attr.name("attr");
        values.name("values");
        item.name("item");

        on_error<fail>
                (
                    attrs
                    , std::cout
                    << val("Error! Expecting ")
                    << _4                               // what failed?
                    << val(" here: \"")
                    << construct<std::string>(_3, _2)   // iterators to error-pos, end
                    << val("\"")
                    << std::endl
                    );

        debug(attrs);
        debug(attr);
        debug(values);
        debug(item);
    }

    qi::rule<Iterator, std::string(), ascii::space_type> attrs, attr, values, item;
};

}


int main(int /*argc*/, char**/* argv*/)
{
    using std::string;
    const std::string data1 {"(a),(b=b1),(c=c1,c2,c3),(d=d\\,1,d2,d\\,3)"};
    const std::string data2 {"a,(b=b1),(c=c1,c2,c3),(d=d1,d2,d3)"};
    const string data3 {"d1,d2,d3"};

    const std::string data = data3;

    using boost::spirit::ascii::space;
    typedef std::string::const_iterator iterator_type;
    typedef client::attributes_parser<iterator_type> attributes;

    attributes attrgram; // Our grammar

    std::string::const_iterator iter = data.begin();
    std::string::const_iterator end = data.end();
    bool r = phrase_parse(iter, end, attrgram, space);
    if (r && iter == end)
    {
        ;/*
        std::cout << "-------------------------\n";
        std::cout << "Parsing succeeded\n";
        std::cout << data << " Parses OK: " << std::endl;
        */
    }
    else
    {
        std::cout << "-------------------------\n";
        std::cout << "Parsing failed\n";
        std::cout << "-------------------------\n";
    }
   
    return EXIT_SUCCESS;
}

