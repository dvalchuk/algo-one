#include "bitutils.h"
#include <bitset>

namespace bitutils {

    std::string bitstostr(const unsigned char* pB, const size_t sz, bool with_separator/* = false*/) {
        if (pB == nullptr || sz == 0) {
            return std::string();
        }
        std::string result;
        for(size_t i = sz; i--;) {
            const unsigned char ch(*(pB + i));
            const std::bitset<sizeof(unsigned char) * 8> bs(ch);
            result += bs.to_string<char, std::char_traits<char>, std::allocator<char>>();
            if (with_separator && i != 0) {
                result += '-';
            }
        }
        return result;
    }

}
