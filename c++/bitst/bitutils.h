#pragma once

#include <string>

namespace bitutils {

    std::string bitstostr(const unsigned char* pB, const size_t sz, bool with_separator = false);

}
