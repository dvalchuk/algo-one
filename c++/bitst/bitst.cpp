#include <cstdlib>
#include <iostream>

#include "bitutils.h"


int main(int, const char** const) {
    unsigned int val = 16;
    std::cout << val << " is " << bitutils::bitstostr(reinterpret_cast<const unsigned char*>(&val), sizeof(val), true) << std::endl;
	return EXIT_SUCCESS;
}

