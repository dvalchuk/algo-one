#ifndef HMAP_H
#define HMAP_H

#include "hfun.hpp"
#include "list.hpp"

namespace a1 {

// TODO:
// implement hash table
// add allocator
// add remove()

template <typename T, class HashFunc = a1::hash<T> >
class hset
{
    enum {
        DefaultSize = 1024
    };
    typedef list<T> list_t;
    typedef list<class list<T>::iterator > iterators_t;
    // TODO: should contain pointers to nodes - to avoid code blow.
    iterators_t* _storage;
    list_t _raw_storage;
    class list<T>::iterator _last_inserted;
    HashFunc _hashFunc;
public:
    typedef class list<T>::iterator iterator;
    typedef class list<T>::const_iterator const_iterator;

    hset() :
        _storage(new iterators_t[hset::DefaultSize]),
        _hashFunc(hset::DefaultSize) {

    }

    explicit hset(std::size_t size) :
        _storage(new iterators_t[size]),
        _hashFunc(size) {
    }

    ~hset() {
        delete [] _storage;
    }

    void insert(const T& value) {
        const std::size_t hash_value = _hashFunc(value);
        iterators_t& internal_list = _storage[hash_value];
        _raw_storage.push_back(value);
        if (_raw_storage.size() == 1U) {
            _last_inserted = _raw_storage.begin();
        }
        else {
            ++_last_inserted;
        }
        internal_list.push_back(_last_inserted);
    }

    iterator begin() {
        return _raw_storage.begin();
    }

    const_iterator begin() const {
        return _raw_storage.begin();
    }

    iterator end() {
        return _raw_storage.end();
    }

    const_iterator end() const {
        return _raw_storage.end();
    }

    iterator find(const T& value) const {
        const std::size_t hash_value = _hashFunc(value);
        const iterators_t& internal_list = _storage[hash_value];
        if (internal_list.empty()) {
            return iterator();
        }
        if (internal_list.size() == 1) {
            return *internal_list.begin();
        }
        for(class iterators_t::const_iterator it = internal_list.begin(), et = internal_list.end(); it != et; ++it) {
            if (*(*it) == value) {
                return *it;
            }
        }
        return iterator();
    }

};

} // a1

#endif // HMAP_H
