TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++0x

SOURCES += main.cpp \

HEADERS += \
    hset.hpp \
    hfun.hpp \
    node.hpp \
    list.hpp \
    list_iterator.hpp

