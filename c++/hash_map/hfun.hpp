#ifndef HFUN_H
#define HFUN_H

namespace a1 {

//TODO: add bits hash function (study murmur algorythm)
template <typename T>
class hash {
    std::size_t _n;

public:
    explicit hash(std::size_t n) :
        _n(n) {
    }

    std::size_t operator() (int key) const {
        return static_cast<std::size_t> (key) % _n;
    }
};

} // a1

#endif // HFUN_H
