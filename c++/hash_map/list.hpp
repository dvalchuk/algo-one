#ifndef LIST_HPP
#define LIST_HPP

#include "node.hpp"
#include "list_iterator.hpp"


namespace a1 {

// TODO:
// add remove()

template <typename T>
class list {
    typedef list<T> self_t;
    typedef detail::node<T> node_t;
    node_t* _head;
    node_t* _last;
    std::size_t _size;
public:
    typedef list_iterator<T> iterator;
    typedef const_list_iterator<T> const_iterator;

    list() :
        _head(nullptr),
        _last(nullptr),
        _size(0U) {
    }

    ~list() {
        node_t* current = _head;
        while (current)
        {
            node_t* cached_next = static_cast<node_t*>(current->_next);
            delete current;
            current = cached_next;
        }
    }

    void push_back(const T& value) {
        node_t* node = new node_t(value);
        if (_head == nullptr) {
            _head = _last = node;
        } else {
            _last->_next = node;
            _last = node;
        }
        ++_size;
    }

    inline iterator begin() {
        return iterator(_head);
    }

    inline const_iterator begin() const {
        return const_iterator(_head);
    }

    inline iterator end() {
        return iterator();
    }

    inline const_iterator end() const {
        return const_iterator();
    }

    iterator find(const T& value) const {
        node_t* current = _head;
        while (current)
        {
            if (current->_value == value)
            {
                return iterator(current);
            }
            current = static_cast<node_t*>(current->_next);
        }
        return iterator();
    }

    inline bool empty() const {
        return (_size == 0U);
    }

    inline std::size_t size() const {
        return _size;
    }

};

}

#endif // LIST_HPP
