#include <iostream>
#include <cstdlib>

#include "hset.hpp"
#include "list.hpp"

using namespace std;

// TODO: add tut testing framework
int main() {
    {
        a1::list<int> ll;
        ll.push_back(21);
        ll.push_back(22);
        ll.push_back(23);
        cout << "list:";
        for(a1::list<int>::const_iterator it = ll.begin(), et = ll.end(); it != et; ++it) {
            cout << " " << *it;
        }
        cout << endl;

        a1::list<int>::iterator it = ll.find(22);
        if (it != ll.end()) {
            cout << "found: " << *it << endl;
        }
    }
    {
        a1::hset <int> hs;
        hs.insert(11);
        hs.insert(-12);
        hs.insert(13);
        cout << "hash_set:";
        for(a1::hset<int>::const_iterator it = hs.begin(), et = hs.end(); it != et; ++it) {
            cout << " " << *it;
        }
        cout << endl;

        a1::hset<int>::iterator it = hs.find(-12);
        if (it != hs.end()) {
            cout << "found: " << *it << endl;
        }
    }

    cout << endl;
    return EXIT_SUCCESS;
}

