#ifndef LIST_ITERATOR_H
#define LIST_ITERATOR_H

#include "node.hpp"

namespace a1 {

// forward only - due to single-linked list

template<typename T>
struct list_iterator {
    typedef detail::node<T> node_t;
    node_t* _node;
    typedef list_iterator<T> self_t;

    list_iterator() :
        _node(nullptr) {
    }

    explicit list_iterator(node_t* node) :
        _node(node) {
    }

    list_iterator(const self_t& rhv) {
        _node = rhv._node;
    }

    self_t& operator ++() {
        if (_node)
        {
            _node = static_cast<node_t*> (_node->_next);
        }
        return *this;
    }

    self_t operator ++(int) {
        self_t cached_iterator(*this);
        ++(*this);
        return cached_iterator;
    }

    T& operator *() const {
        return _node->_value;
    }

    T* operator ->() const {
        return &_node->_value;
    }

};

template <typename T>
inline bool operator ==(const list_iterator<T>& lhv, const list_iterator<T>& rhv) {
    return lhv._node == rhv._node;
}

template <typename T>
bool operator !=(const list_iterator<T>& lhv, const list_iterator<T>& rhv) {
    return !(lhv == rhv);
}


template<typename T>
struct const_list_iterator {
    typedef detail::node<T> node_t;
    node_t* _node;
    typedef const_list_iterator<T> self_t;
    typedef list_iterator<T> mutable_self_t;

    const_list_iterator() :
        _node(nullptr) {
    }

    explicit const_list_iterator(node_t* node) :
        _node(node) {
    }

    const_list_iterator(const self_t& rhv) {
        _node = rhv._node;
    }

    const_list_iterator(const mutable_self_t& rhv) {
        _node = rhv._node;
    }

    self_t& operator ++() {
        if (_node)
        {
            _node = static_cast<node_t*> (_node->_next);
        }
        return *this;
    }

    self_t operator ++(int) {
        self_t cached_iterator(*this);
        ++(*this);
        return cached_iterator;
    }

    const T& operator *() const {
        return _node->_value;
    }

    const T* operator ->() const {
        return &_node->_value;
    }

};

template <typename T>
inline bool operator ==(const const_list_iterator<T>& lhv, const const_list_iterator<T>& rhv) {
    return lhv._node == rhv._node;
}

template <typename T>
bool operator !=(const const_list_iterator<T>& lhv, const const_list_iterator<T>& rhv) {
    return !(lhv == rhv);
}

}


#endif // LIST_ITERATOR_H
