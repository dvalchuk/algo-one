#ifndef NODE_HPP
#define NODE_HPP

namespace a1 {

namespace detail {

struct node_base {
    node_base* _next;

    node_base() :
        _next(nullptr) {
    }

    bool last() const {
        return (_next == nullptr);
    }

};

template <typename V>
struct node : public node_base {
    V _value;

    node(const V& value) :
        node_base(),
        _value(value) {
    }
};

}

}

#endif // NODE_HPP
