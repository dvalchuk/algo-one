#include "global.h"
#include "call_foo_if_exist.h"
#include "a.h"
#include "b.h"

#include <cstdlib>
#include <cstdio>


// Call method if it exists test
int main(int argc, char *argv[])
{
    SF_UNUSED(argc);
    SF_UNUSED(argv);

    printf("main\n");
    A a;
    B b;
    
    const int ra = processObject<int>(a);
    const int rb = processObject<int>(b);
    printf("ra: %d, rb: %d\n", ra, rb);

    return EXIT_SUCCESS;
}
