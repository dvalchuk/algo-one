#ifndef GLOBAL_H
#define GLOBAL_H

#pragma once

#define SF_UNUSED(x) (void)x;
#if defined(_MSC_VER)
# define SF_FUNC_INFO __FUNCSIG__
#else
# define SF_FUNC_INFO __PRETTY_FUNCTION__
#endif

#endif // GLOBAL_H
