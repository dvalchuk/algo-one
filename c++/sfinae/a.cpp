#include "a.h"
#include "global.h"

#include <cstdio>

A::A()
{
    printf("%s with foo()\n", SF_FUNC_INFO);
}

int A::foo()
{
    printf("%s\n", SF_FUNC_INFO);
    return 500;
}
