#ifndef CALL_FOO_IF_EXIST_H
#define CALL_FOO_IF_EXIST_H

#pragma once

#include "sfinae.h"
#include "global.h"


template<bool C, typename T = void>
struct enableIf {
  typedef T type;
};

template<typename T>
struct enableIf<false, T> { };

SF_HAS_METHOD(foo, hasFoo);

template<typename R, typename T>
typename enableIf<hasFoo<T, R(T::*)()>::value, R>::type
processObject(T& o)
{
   return o.foo();
}

template<typename R, typename T>
typename enableIf<!hasFoo<T, R(T::*)()>::value, R>::type
processObject(T& o)
{
    SF_UNUSED(o);
    return 0;
}


#endif // CALL_FOO_IF_EXIST_H
