QT     += core
QT     -= gui

TARGET = sfinae
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    a.cpp \
    b.cpp

HEADERS += \
    sfinae.h \
    a.h \
    b.h \
    global.h \
    call_foo_if_exist.h
