#ifndef A_H
#define A_H

#pragma once

class A
{
public:
    A();

    int foo();
};

#endif // A_H
