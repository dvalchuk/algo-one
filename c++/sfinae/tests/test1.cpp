#include <tut/tut.hpp>
#include <tut/tut_main.hpp>
#include <tut/tut_console_reporter.hpp>

#include <exception>
#include <iostream>
#include <cstdlib>

#include "a.h"
#include "b.h"
#include "call_foo_if_exist.h"

namespace tut
{

    struct test
    {
        virtual ~test()
        {
        }
    };

    typedef test_group<test> tg;
    typedef tg::object object;
    tg test1("sfinae tests");

    template<>
    template<>
    void object::test<1>()
    {
        A a;
        B b;
        const int ra = processObject<int>(a);
        ensure("a has foo() which should receive positive value", ra > 0);
        const int rb = processObject<int>(b);
        ensure_equals("b does not have foo()", rb, 0);
    }


    test_runner_singleton runner;
}

int main(int argc, const char *argv[])
{
    using namespace std;
    tut::console_reporter reporter(std::cout);
    tut::runner.get().set_callback(&reporter);

    try
    {
        if(tut::tut_main(argc, argv))
        {
            if(reporter.all_ok())
            {
                return EXIT_SUCCESS;
            }
            else
            {
                return EXIT_FAILURE;
            }
        }
    }
    catch(const tut::no_such_group &ex)
    {
        std::cerr << "No such group: " << ex.what() << std::endl;
    }
    catch(const tut::no_such_test &ex)
    {
        std::cerr << "No such test: " << ex.what() << std::endl;
    }
    catch(const tut::tut_error &ex)
    {
        std::cout << "General error: " << ex.what() << std::endl;
    }

    return EXIT_SUCCESS;
}
