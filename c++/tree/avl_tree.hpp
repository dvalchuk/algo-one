#ifndef AVL_TREE_HPP
#define AVL_TREE_HPP

#include <algorithm>

namespace a1 {

namespace detail {

template <typename K>
class comparator {
    K _key;
public:
    enum equality
    {
        LESS = -1,
        EQUAL = 0,
        MORE = 1
    };

    comparator(const K& key) :
        _key(key) {
    }

    equality operator() (const K& key) {
        if (_key < key) {
            return LESS;
        }
        if (key < _key) {
            return MORE;
        }
        return EQUAL;
    }
};

}

template <typename K, typename V, class Comparator = detail::comparator<K> >
class avl_tree {
    typedef class Comparator comparator_t;

    template <typename K, typename V>
    struct node {
        enum direction {
            LEFT = 0,
            RIGHT = 1
        };

        typedef node<K, V> self_t;

        K _key;
        V _value;
        self_t* _children[2];
        short _balance;

        node(const K& key, const V& value) :
            _key(key),
            _value(value) {
        }

        void rotate_left(self_t* root) {
            self_t* old_root = root;
            root = root->_children[RIGHT];
            old_root->_children[RIGHT] = root->_children[LEFT];
            root->_children[LEFT] = old_root;

            old_root->_balance -= (1 + std::max(root->_balance, 0));
            root->_balance -= (1 - std::min(old_root->_balance, 0));
        }

        void rotate_right(self_t* root) {
            self_t* old_root = root;
            root = root->_children[LEFT];
            old_root->_children[LEFT] = root->_children[RIGHT];
            root->_children[RIGHT] = old_root;

            old_root->_balance += (1 - std::min(root->_balance, 0));
            root->_balance += (1 + std::max(old_root->_balance, 0));
        }

    };
};

}


#endif // AVL_TREE_HPP
