#include <boost/python.hpp>

#include <cstdio>

namespace some {
    void foo(const char* from)
    {
        printf("some::foo(%s)\n", from);
    }

    void foo1()
    {
        printf("some::foo1()\n");
    }
} // namespace some

BOOST_PYTHON_MODULE(some)
{
    using namespace boost::python;
    def("foo", some::foo);
    def("foo1", some::foo1);
}
